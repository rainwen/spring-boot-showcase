package com.rainwen.springcache.annotation;

import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Caching;

import java.lang.annotation.*;

/**
 * Created by rain.wen on 2017/7/13.
 */
@Caching(
        put = {
                @CachePut(value = "user", key = "#user.id", unless = "#result eq null"),
                @CachePut(value = "user", key = "#user.username", unless = "#result eq null"),
                @CachePut(value = "user", key = "#user.email", unless = "#result eq null")
        }
)
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface UserSaveCache {
}
