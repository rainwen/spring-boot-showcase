package com.rainwen.springcache;


import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.context.annotation.Configuration;

/**
 * Created by rain.wen on 2017/8/9.
 *
 *  https://docs.spring.io/spring/docs/current/spring-framework-reference/html/cache.html
 *  http://jinnianshilongnian.iteye.com/blog/2001040 完善测试
 *
 *  https://www.jianshu.com/p/275cb42080d9 单个缓存刷新方案
 */
@Configuration
@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }


}
