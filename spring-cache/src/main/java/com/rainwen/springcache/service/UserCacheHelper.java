package com.rainwen.springcache.service;

import com.rainwen.springcache.entity.User;
import org.springframework.cache.ehcache.EhCacheCache;

/**
 * 缓存辅助类
 * Created by rain.wen on 2017/7/13.
 */
public class UserCacheHelper {

    /**
     * 是否能失效 !方法必须为static
     * @param cache 缓存对象
     * @param id 缓存key
     * @param username 用户名
     * @return
     */
    public static boolean canEvict(EhCacheCache cache, Integer id, String username) {
        User cacheUser = cache.get(id, User.class);
        if (cacheUser == null) {
            return false;
        }
        return !cacheUser.getUsername().equals(username);
    }

}
