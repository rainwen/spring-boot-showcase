package com.rainwen.springcache.service;

import com.rainwen.springcache.annotation.UserSaveCache;
import com.rainwen.springcache.entity.User;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * Created by rain.wen on 2017/7/5.
 */
@Service
public class UserService {

    /**
     * 根据id查询
     *
     * @param id
     * @return
     */
    @Cacheable(value = "user", key = "#id", unless = "#result eq null")
    public User findById(Integer id) {
        System.out.println("==> findById from DB");
        User user = new User();
        user.setId(id);
        user.setUsername("rainwen");
        return user;
    }

    /**
     * 根据username查询
     *
     * @param username
     * @return
     */
    @Cacheable(value = "user", key = "#username", unless = "#result eq null")
    public User findByUsername(String username) {
        System.out.println("==> findByUsername from DB");
        User user = new User();
        user.setId(1);
        user.setUsername(username);
        return user;
    }

    /**
     * 根据username查询
     * 当结果不为空,将id/username/email作为条件也加入缓存
     *
     * @param username
     * @return
     */
    @Caching (
            put = {
                    @CachePut(value = "user", key = "#result.id", unless = "#result eq null"),
                    @CachePut(value = "user", key = "#result.username", unless = "#result eq null"),
                    @CachePut(value = "user", key = "#result.email", unless = "#result eq null")
            }
    )
    public User findByUsername2(String username) {
        System.out.println("==> findByUsername2 from DB");
        User user = new User();
        user.setId(1);
        user.setUsername(username);
        user.setEmail("rain.wenjl@gmail.com");
        return user;
    }

    /**
     * 保存用户, 同时保存id/username/email 缓存
     *
     * @param user
     * @return
     */
    @Caching(
            put = {
                    @CachePut(value = "user", key = "#user.id", unless = "#result eq null"),
                    @CachePut(value = "user", key = "#user.username", unless = "#result eq null"),
                    @CachePut(value = "user", key = "#user.email", unless = "#result eq null")
            }
    )
    public User save(User user) {
        System.out.println("==>　save user to DB");
        user.setId(1);
        return user;
    }

    /**
     * 使用外部缓存注解
     * @param user
     * @return
     */
    @UserSaveCache
    public User save2(User user) {
        System.out.println("==>　save user to DB");
        user.setId(1);
        return user;
    }

    /**
     * 更新用户信息,按条件失效缓存
     * 当更新用户名匹配则失效缓存
     * @param user
     */
    @CacheEvict(value = "user", key = "#user.id",
            condition = "T(com.rainwen.springcache.service.UserCacheHelper).canEvict(#root.caches[0],#user.id, #user.username)",
            beforeInvocation = true)
    public void updateUser(User user) {
        System.out.println("==> update cache evict ");
    }

    /**
     * 删除用户,同时删除id/username/email 缓存
     *
     *
     * @param user
     */
    @Caching(
            evict = {
                    @CacheEvict(value = "user", key = "#user.id"),
                    @CacheEvict(value = "user", key = "#user.email"),
                    @CacheEvict(value = "user", key = "#user.username")
            }
    )
    public void deleteUser(User user) {
        System.out.println("==>　delete user from DB");
    }

    /**
     * 删除 user 所有缓存
     */
    @CacheEvict(value = "user", allEntries = true)
    public void deleteAll() {
        System.out.println("==>　delete all user from DB");
    }

}
