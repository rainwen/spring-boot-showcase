package com.rainwen.springcache.service;

import com.rainwen.springcache.Application;
import com.rainwen.springcache.entity.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import java.util.Date;

/**
 * 缓存测试
 * Created by rain.wen on 2017/7/5.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
public class UserServiceTest {

    @Autowired
    private UserService userService;

    Integer id = 1;
    String username = "rainwen";
    String email = "rain.wenjl@gmail.com";
    String mobile = "18888888888";

    @Test
    public void saveTest() {
        User user = getDemoUser();
        userService.save(user);

        Integer id = user.getId();

        System.out.println("==> from cache by id");
        User user1 = userService.findById(id);
        Assert.isTrue(user1.getId().intValue() == id, "");

        System.out.println("==> from cache by username");
        User user2 = userService.findByUsername(username);
        Assert.isTrue(user2.getId().intValue() == id, "");
    }

    @Test
    public void save2Test() {
        User user = getDemoUser();
        userService.save2(user);

        Integer id = user.getId();

        System.out.println("==> from cache by id");
        User user1 = userService.findById(id);
        Assert.isTrue(user1.getId().intValue() == id, "");

        System.out.println("==> from cache by username");
        User user2 = userService.findByUsername(username);
        Assert.isTrue(user2.getId().intValue() == id, "");
    }

    @Test
    public void findPutCacheTest() {
        System.out.println("==> 1 ");
        User user2 = userService.findByUsername(username);
        Assert.isTrue(user2.getId().intValue() == id, "");
        System.out.println("==> 1 end");

        System.out.println("==> 2");
        user2 = userService.findByUsername(username);
        Assert.isTrue(user2.getId().intValue() == id, "");
        System.out.println("==> 2 end");

    }

    @Test
    public void findPutAllKeyToCacheTest() {
        System.out.println("==> 1 ");
        User user2 = userService.findByUsername2(username);
        Assert.isTrue(user2.getId().intValue() == id, "");
        System.out.println("==> 1 end");

        System.out.println("==> 2");
        user2 = userService.findById(id);
        Assert.isTrue(user2.getId().intValue() == id, "");
        System.out.println("==> 2 end");
    }

    @Test
    public void evictCacheTest() {
        System.out.println("==> 1 ");
        User user = userService.findByUsername(username);
        Assert.isTrue(user.getId().intValue() == id, "");
        System.out.println("==> 1 end");

        System.out.println("==> 2 ");
        User user2 = userService.findByUsername(username);
        Assert.isTrue(user2.getId().intValue() == id, "");
        System.out.println("==> 2 end");

        userService.deleteUser(user);

        System.out.println("==> 3 ");
        User user3 = userService.findByUsername(username);
        Assert.isTrue(user3.getId().intValue() == id, "");
        System.out.println("==> 3 end");
    }

    @Test
    public void evictAllCacheTest() {
        System.out.println("==> 1 ");
        User user = userService.findByUsername(username);
        Assert.isTrue(user.getId().intValue() == id, "");
        System.out.println("==> 1 end");

        System.out.println("==> 2 ");
        User user2 = userService.findByUsername(username);
        Assert.isTrue(user2.getId().intValue() == id, "");
        System.out.println("==> 2 end");

        userService.deleteAll();

        System.out.println("==> 3 ");
        User user3 = userService.findByUsername(username);
        Assert.isTrue(user3.getId().intValue() == id, "");
        System.out.println("==> 3 end");
    }

    @Test
    public void updateUserTest() {
        System.out.println("==> 1 put cache");
        User user = userService.save(getDemoUser());
        Assert.isTrue(user.getId().intValue() == id, "");
        System.out.println("==> 1 end");

        System.out.println("==> 2");
        User user2 = new User();
        BeanUtils.copyProperties(user, user2);
        user2.setUsername("updatecache");
        userService.updateUser(user2);
        System.out.println("==> 2 end");

        System.out.println("==> 3 no cache");
        User user3 = userService.findById(id);
        Assert.isTrue(user3.getId().intValue() == id, "");
        System.out.println("==> 3 end");
    }

    private User getDemoUser(){
        User user = new User();
        user.setUsername(username);
        user.setEmail(email);
        user.setMobile(mobile);
        user.setCreateTime(new Date());
        return user;
    }
}
