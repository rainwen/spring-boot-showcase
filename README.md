### [spring-boot-showcase](https://gitee.com/rainwen/spring-boot-showcase) 项目介绍
**spring-boot-showcase** 主要包含 **Spring-Boot** 一些集成案例，基于Spring-Boot 1.5.3.RELEASE 版本

### 子模块介绍

#### [spring-boot-jsp](https://gitee.com/rainwen/spring-boot-showcase/tree/master/spring-boot-jsp)

spring-boot-jsp 主要实现 Spring-Boot 内嵌 Tomcat 实现JSP访问，并解决启动：Whitelabel Error Page (type=Not Found, status=404) 问题

详情参考 [博客](https://my.oschina.net/wenjinglian/blog/1506808)

#### [spring-boot-rpc](https://gitee.com/rainwen/spring-boot-showcase/tree/master/spring-boot-rpc)

spring-boot-rpc 主要实现分布式远程调用功能，目前集成了dubbo功能，注册中心采用的是Zookeeper

项目包含3个模块<br/>
|_ spring-boot-api 接口定义模块<br/>
|_ spring-boot-consumer 服务的调用模块，在这部分方法调用加入Hystrix 熔断机制<br/>
|_ spring-boot-dubbo-provider dubbo服务提供者模块<br/>

#### [spring-boot-shiro](https://gitee.com/rainwen/spring-boot-showcase/tree/master/spring-boot-shiro)

spring-boot-shiro 主要实现与shiro集成，在spring-boot-shiro-velocity 模块中包含完整的用户、角色、权限功能，实现了数据入库和简单的登录功能。

#### [spring-cache](https://gitee.com/rainwen/spring-boot-showcase/tree/master/spring-cache)

spring-cache 主要使用spring cache的注解功能，目前只在本地实现单元测试功能，缓存提供者使用的是Ehcache，切换成其他缓存（Redis）也比较简单。
Spring 定义一套缓存的标准，主要使用的注解@Cacheable(缓存注解)、@Caching(缓存注解组合注解)、@CacheEvict(缓存失效注解)，具体使用查看代码。

#### [spring-rabbitmq](https://gitee.com/rainwen/spring-boot-showcase/tree/master/spring-rabbitmq)

spring-rabbitmq 主要实现包括：
1. 对 RabbitMQ 消息队列的初始化、管理
2. 不同交换机消息对列的使用
3. 提供 RabbitMQ 外部接口消息接口
4. RabbitMQ 在企业集成中应用的单元测试

#### [spring-mybatis](https://gitee.com/rainwen/spring-boot-showcase/tree/master/spring-mybatis)

spring-mybatis 实现Spring-Boot 与 MyBatis 的集成，在本项目中还实现了主从读写分离功能。

详情参考 [博客](https://my.oschina.net/wenjinglian/blog/1502414)


### [spring-boot-trace](https://gitee.com/rainwen/spring-boot-showcase/tree/master/spring-boot-trace)

分布式调用监控必不可少，spring-boot-trace 分布式调用链追踪集成

目前已经实现了 Spring-Boot 和 Zipkin 集成 [spring-boot-zipkin](https://gitee.com/rainwen/spring-boot-showcase/tree/master/spring-boot-trace/spring-boot-zipkin)


