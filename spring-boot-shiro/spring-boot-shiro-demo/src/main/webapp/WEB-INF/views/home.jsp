<%--
  Created by IntelliJ IDEA.
  User: rainwen
  Date: 2017/8/9
  Time: 19:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<html>
<head>
    <title></title>
</head>
<body>

【<shiro:principal type="java.lang.String"/>】 欢迎您，我的主人  <a href="/logout">【退出】</a>
<br/>
<br/>
<shiro:hasRole name="user">
  <shiro:hasPermission name="user:create">
    拥有user角色, 拥有create <a href="/user/add">【添加用户操作】</a>
  </shiro:hasPermission>
<br/>
  <shiro:hasPermission name="user:update">
    拥有user角色, 拥有update <a href="/user/edit/1">【更新用户操作】</a>
  </shiro:hasPermission>
<br/>
  <shiro:hasPermission name="user:delete">
    拥有user角色, 拥有delete <a href="/user/delete/1">【删除用户操作】</a>
  </shiro:hasPermission>

</shiro:hasRole>

<br/>
<shiro:hasPermission name="user:view">
  拥有user:view权限才显示,必须拥有user角色才能操作 <a href="/user/detail/1">【查看用户操作】</a>
</shiro:hasPermission>

<br/>
拥有user:print权限才能操作 <a href="/user/print">【打印操作】</a>


</body>
</html>
