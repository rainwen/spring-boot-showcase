package com.rainwen.shiro.service;

import com.rainwen.shiro.entity.Role;

/**
 * Created by rain.wen on 2017/8/9.
 */
public class RoleService {

    public Role createRole(Role role) {
        return role;
    }

    public void deleteRole(Long roleId) {

    }

    /**
     * 添加角色-权限之间关系
     * @param roleId
     * @param permissionIds
     */
    public void correlationPermissions(Long roleId, Long... permissionIds) {

    }

    /**
     * 移除角色-权限之间关系
     * @param roleId
     * @param permissionIds
     */
    public void uncorrelationPermissions(Long roleId, Long... permissionIds) {

    }
}
