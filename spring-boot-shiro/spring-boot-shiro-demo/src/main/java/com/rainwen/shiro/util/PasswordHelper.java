package com.rainwen.shiro.util;

import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.util.ByteSource;

/**
 * 密码辅助类
 *
 * @author rain.wen
 */
public class PasswordHelper {

    private static String algorithmName = "MD5";
    /**
     * 加密迭代次数
     */
    private static final int HASH_ITERATIONS = 2;

    /**
     * 加密
     *
     * @param plainPassword
     * @param salt
     * @return
     */
    public static String encryptPassword(String plainPassword, String salt) {
        String newPassword = new SimpleHash(
                algorithmName,
                plainPassword,
                ByteSource.Util.bytes(salt),
                HASH_ITERATIONS).toHex();
        return newPassword;
    }

}