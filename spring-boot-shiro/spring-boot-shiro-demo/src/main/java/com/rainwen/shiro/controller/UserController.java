package com.rainwen.shiro.controller;


import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by rain.wen on 2017/8/17.
 */
@Controller
@RequestMapping("/user")
public class UserController {

    /**
     * 查询列表
     * @return
     */
    @RequestMapping("/list")
    public String list() {

        return "user/list";
    }

    /**
     * 进入新增页
     * @return
     */
    @RequestMapping(value = "/add",method = RequestMethod.GET)
    public String add() {

        return "user/add";
    }

    /**
     * 保存
     * @return
     */
    @RequestMapping(value = "/add",method = RequestMethod.POST)
    @ResponseBody
    public String save() {
        return "success";
    }

    /**
     * 进入编辑
     * @return
     */
    @RequestMapping(value = "/edit/{id}",method = RequestMethod.GET)
    public String edit(@PathVariable Integer id) {
        return "user/edit";
    }

    /**
     * 编辑保存
     * @return
     */
    @RequestMapping(value = "/edit",method = RequestMethod.POST)
    @ResponseBody
    public String edit() {
        return "success";
    }

    /**
     * 删除
     * @return
     */
    @RequestMapping(value = "/delete/{id}",method = RequestMethod.POST)
    @ResponseBody
    public String delete(@PathVariable Integer id) {
        return "success";
    }

    /**
     * 查看详情
     * @param id
     * @return
     */
    @RequestMapping(value = "/detail/{id}",method = RequestMethod.GET)
    public String detail(@PathVariable Integer id) {
        return "user/view";
    }

    /**
     * 打印
     * @return
     */
    @RequestMapping(value = "/print",method = RequestMethod.GET)
    public String print() {
        return "user/print";
    }
}
