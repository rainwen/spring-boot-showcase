package com.rainwen.shiro.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Shiro默认配置
 * @author rain.wen
 * @date 2018/1/9.
 */
@ConfigurationProperties(prefix = "shiro")
public class ShiroSettings {

    private String defaultPermissionString;
    private String defaultRoleString;
    private String casServerUrlPrefix;
    private String loginUrl;
    private String logoutUrl;
    private String serviceUrl;
    private String successUrl;
    private String errorUrl;
    private String unauthorizedUrl;
    private String filterChainDefinitions;
    private String appCode;
    private String redisHost;
    private String redisPassword;
    private int redisPort;
    private int redisExpire;

    public String getRedisHost() {
        return redisHost;
    }

    public void setRedisHost(String redisHost) {
        this.redisHost = redisHost;
    }

    public String getRedisPassword() {
        return redisPassword;
    }

    public void setRedisPassword(String redisPassword) {
        this.redisPassword = redisPassword;
    }

    public int getRedisPort() {
        return redisPort;
    }

    public void setRedisPort(int redisPort) {
        this.redisPort = redisPort;
    }

    public int getRedisExpire() {
        return redisExpire;
    }

    public void setRedisExpire(int redisExpire) {
        this.redisExpire = redisExpire;
    }

    public String getDefaultPermissionString() {
        return defaultPermissionString;
    }

    public void setDefaultPermissionString(String defaultPermissionString) {
        this.defaultPermissionString = defaultPermissionString;
    }

    public String getDefaultRoleString() {
        return defaultRoleString;
    }

    public void setDefaultRoleString(String defaultRoleString) {
        this.defaultRoleString = defaultRoleString;
    }


    public String getCasServerUrlPrefix() {
        return casServerUrlPrefix;
    }

    public void setCasServerUrlPrefix(String casServerUrlPrefix) {
        this.casServerUrlPrefix = casServerUrlPrefix;
    }

    public String getLoginUrl() {
        return loginUrl;
    }

    public void setLoginUrl(String loginUrl) {
        this.loginUrl = loginUrl;
    }

    public String getLogoutUrl() {
        return logoutUrl;
    }

    public void setLogoutUrl(String logoutUrl) {
        this.logoutUrl = logoutUrl;
    }

    public String getServiceUrl() {
        return serviceUrl;
    }

    public void setServiceUrl(String serviceUrl) {
        this.serviceUrl = serviceUrl;
    }

    public String getErrorUrl() {
        return errorUrl;
    }

    public void setErrorUrl(String errorUrl) {
        this.errorUrl = errorUrl;
    }

    public String getUnauthorizedUrl() {
        return unauthorizedUrl;
    }

    public void setUnauthorizedUrl(String unauthorizedUrl) {
        this.unauthorizedUrl = unauthorizedUrl;
    }

    public String getFilterChainDefinitions() {
        return filterChainDefinitions;
    }

    public void setFilterChainDefinitions(String filterChainDefinitions) {
        this.filterChainDefinitions = filterChainDefinitions;
    }

    public String getAppCode() {
        return appCode;
    }

    public void setAppCode(String appCode) {
        this.appCode = appCode;
    }

    public String getSuccessUrl() {
        return successUrl;
    }

    public void setSuccessUrl(String successUrl) {
        this.successUrl = successUrl;
    }
}
