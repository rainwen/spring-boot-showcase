package com.rainwen.shiro.enums;

/**
 * Created by rain.wen on 2017/8/9.
 */
public enum UserStatus {

    NORMAL(0),
    LOCKED(1),
    DISABLE(2);

    private int key;

    private UserStatus(int key) {
        this.key = key;
    }

    public int getKey() {
        return key;
    }
}
