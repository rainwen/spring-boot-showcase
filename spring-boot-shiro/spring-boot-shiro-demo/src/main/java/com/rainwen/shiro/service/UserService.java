package com.rainwen.shiro.service;

import com.google.common.collect.Sets;
import com.rainwen.shiro.entity.User;
import com.rainwen.shiro.enums.UserStatus;
import com.rainwen.shiro.util.PasswordHelper;
import org.apache.shiro.authc.credential.DefaultPasswordService;
import org.springframework.stereotype.Service;

import java.util.Set;

/**
 * Created by rain.wen on 2017/8/9.
 */
@Service
public class UserService {

    /**
     * 创建账户
     *
     * @param user
     * @return
     */
    public User createUser(User user) {
        return user;
    }

    /**
     * 修改密码
     *
     * @param userId
     * @param newPassword
     */
    public void changePassword(Long userId, String newPassword) {
//TODO
    }

    /**
     * 添加用户-角色关系
     *
     * @param userId
     * @param roleIds
     */
    public void correlationRoles(Long userId, Long... roleIds) {
//TODO
    }

    /**
     * 移除用户-角色关系
     *
     * @param userId
     * @param roleIds
     */
    public void uncorrelationRoles(Long userId, Long... roleIds) {
//TODO
    }

    /**
     * 根据用户名查找用户
     * @param username
     * @return
     */
    public User findByUsername(String username) {
        if ("rainwen".equals(username)) {
            User user = new User();
            user.setId(1L);
            user.setNickName("无忌");
            user.setUsername("rainwen");
            user.setSalt("sadfJKLL");
            user.setPassword(PasswordHelper.encryptPassword("123456", user.getSalt()));
            user.setStatus(UserStatus.NORMAL.getKey());
            return user;
        } else if("guest".equals(username)){
                User user = new User();
                user.setId(2L);
                user.setNickName("访客");
                user.setUsername("guest");
                user.setSalt("sadfJKLL");
                user.setPassword(PasswordHelper.encryptPassword("123456", user.getSalt()));
                user.setStatus(UserStatus.NORMAL.getKey());
                return user;
        }
        return new User();
    }

    /**
     * 根据用户名查找其角色
     *
     * @param username
     * @return
     */
    public Set<String> findRoles(String username) {
        Set<String> roles = Sets.newLinkedHashSet();
        if("rainwen".equals(username)) {
            roles.add("user");
        } else {
            roles.add("guest");
        }
        return roles;
    }

    /**
     * 根据用户名查找其权限
     * system:user:create,delete,update,view
     *
     *
     * @param username
     * @return
     */
    public Set<String> findPermissions(String username) {
        Set<String> permissions = Sets.newLinkedHashSet();
        if("rainwen".equals(username)) {
            permissions.add("user:*");
        } else {
            permissions.add("user:view");
        }
        return permissions;
    }
}
