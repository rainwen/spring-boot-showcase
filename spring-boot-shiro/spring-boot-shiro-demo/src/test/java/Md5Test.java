import java.io.UnsupportedEncodingException;
import java.security.SignatureException;

import com.rainwen.shiro.util.PasswordHelper;
import org.apache.commons.codec.digest.DigestUtils;
import org.junit.Assert;
import org.junit.Test;

/**
 * 标准的MD5 测试
 * 结果对比：http://www.cmd5.com/hash.aspx
 * Created by rain.wen on 2017/8/15.
 */
public class Md5Test {

    /**
     * FIXME 一直没想通为啥PasswordHelper 2次加密与标准的MD5结果不一样 ？？？？
     */
    @Test
    public void testMd5() {
        String password = "123456";
        String salt = "";
        String oneMd5 = md5(password + salt, null);
        System.out.println(oneMd5);
        String result = md5(oneMd5, null);
        System.out.println(result);
        Assert.assertEquals("md5(md5($pass.$salt))", result, PasswordHelper.encryptPassword(password, salt));


    }



    public static String md5(String text, String charset){
        return DigestUtils.md5Hex(getContentBytes(text, charset));
    }

    /**
     * @param content
     * @param charset
     * @return
     * @throws SignatureException
     * @throws UnsupportedEncodingException
     */
    private static byte[] getContentBytes(String content, String charset) {
        if (charset == null || "".equals(charset)) {
            return content.getBytes();
        }
        try {
            return content.getBytes(charset);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("MD5签名过程中出现错误,指定的编码集不对,您目前指定的编码集是:" + charset);
        }
    }
}
