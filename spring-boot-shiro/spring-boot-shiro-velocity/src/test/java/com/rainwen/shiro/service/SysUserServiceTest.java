package com.rainwen.shiro.service;

import com.rainwen.shiro.Application;
import com.rainwen.shiro.entity.SysUser;
import com.rainwen.shiro.util.PasswordHelper;
import com.rainwen.shiro.vo.UrlPermissionVO;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author rain.wen
 * @date 2018/1/15.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
//@Transactional
@Rollback
public class SysUserServiceTest {

    @Autowired
    private SysUserService sysUserService;

    @Autowired
    private SysRoleService sysRoleService;

    String nickname = "无忌";
    String username = "rainwen";
    String password = "123456";
    Integer adminstrator = 1;

    List<Integer> roleIds = new ArrayList<Integer>(){{
        add(adminstrator);
        add(2);
    }};

    List<Integer> resourceIds = new ArrayList<Integer>(){{
        add(1);
        add(2);
        add(3);
        add(4);
        add(5);
    }};

    public void initData() throws Exception {
        sysUserService.addSysUser(username, nickname, password);
        findUrlPermissions();

    }

    @Test
    public void addSysUser() throws Exception {
        sysUserService.addSysUser(username, nickname, password);
        SysUser sysUser = sysUserService.findByUsername(username);

        Assert.assertEquals(sysUser.getUsername(), username);
    }

    @Test
    public void changePassword() throws Exception {
        SysUser sysUser = sysUserService.addSysUser(username, nickname, password);
        String newPassword = "654321";
        sysUserService.changePassword(sysUser.getId(), newPassword);

        sysUser = sysUserService.findByUsername(username);

        Assert.assertEquals(sysUser.getPassword(), PasswordHelper.encryptPassword(newPassword, sysUser.getSalt()));
    }

    @Test
    public void addUserRoles() throws Exception {
        sysUserService.addSysUser(username, nickname, password);
        SysUser sysUser = sysUserService.findByUsername(username);

        int effectRows = sysUserService.addUserRoles(sysUser.getId(), roleIds);
        Assert.assertEquals(effectRows, roleIds.size());
    }

    @Test
    public void deleteUserRoles() throws Exception {
        addUserRoles();

        SysUser sysUser = sysUserService.findByUsername(username);

        int effectRows = sysUserService.deleteUserRoles(sysUser.getId(), roleIds);
        Assert.assertEquals(effectRows, roleIds.size());

    }

    @Test
    public void findByUsername() throws Exception {
        SysUser sysUser = sysUserService.findByUsername(username);
        Assert.assertEquals(sysUser.getUsername(), username);
        Assert.assertEquals(sysUser.getNickname(), nickname);
    }

    @Test
    public void findRoles() throws Exception {
        addUserRoles();

        Set<String> roles = sysUserService.findRoles(username);
        Assert.assertEquals(roles.size(), roleIds.size());
        Assert.assertEquals(roles.contains("administrator"), true);
        Assert.assertEquals(roles.contains("operation"), true);
    }

    @Test
    public void findResourcePermissions() throws Exception {
        Assert.assertEquals(sysRoleService.addRoleResource(adminstrator, resourceIds), resourceIds.size());

        addUserRoles();

        Set<String> permissions = sysUserService.findResourcePermissions(username);
        Assert.assertEquals(permissions.size() > 0, true);
    }

    @Test
    public void findUrlPermissions() throws Exception {
        Assert.assertEquals(sysRoleService.addRoleResource(adminstrator, resourceIds), resourceIds.size());

        addUserRoles();

        List<UrlPermissionVO> urlPermissionVOList = sysUserService.findUrlPermissions(username);
        Assert.assertEquals(urlPermissionVOList.size() > 0 , true);
    }

    @Test
    public void findRoleIds() throws Exception {
        addUserRoles();
        List<Integer> roleIds1 = sysUserService.findRoleIds(username);
        Assert.assertEquals(roleIds1.contains(roleIds.get(0)), true);
        Assert.assertEquals(roleIds1.contains(roleIds.get(1)), true);
    }

}