package com.rainwen.shiro.service;

import com.alibaba.fastjson.JSONObject;
import com.rainwen.shiro.Application;
import com.rainwen.shiro.vo.ResourcePermissionVO;
import com.rainwen.shiro.vo.ResourceVO;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

/**
 * @author rain.wen
 * @date 2018/1/16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
public class SysResourceServiceTest {

    @Autowired
    private SysResourceService sysResourceService;

    @Test
    public void findMenuResourceByUserId() throws Exception {
        Integer userId = 23;
        ResourcePermissionVO resourcePermissionVO = sysResourceService.findMenuResourceByUserId(userId);
        System.out.println(JSONObject.toJSONString(resourcePermissionVO));
        Assert.assertEquals(resourcePermissionVO != null, true);
    }

}