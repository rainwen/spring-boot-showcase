package com.rainwen.shiro.enums;

/**
 * 结果枚举对象
 * @author rain.wen
 * @date 2015/6/4
 *
 */
public enum ResultEnum {
    SUCCESS("0", "成功"),
    FAIL("1", "失败"),
    PARAM_ERROR("2", "参数错误");


    private String result;
    private String msg;

    public String getResult() {
        return result;
    }

    public String getMsg() {
        return msg;
    }

    ResultEnum(String result, String msg) {
        this.result = result;
        this.msg = msg;
    }
}
