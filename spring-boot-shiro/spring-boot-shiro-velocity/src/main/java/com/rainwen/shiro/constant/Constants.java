package com.rainwen.shiro.constant;

/**
 * 常量
 * @author rain.wen
 * @date 2018/1/12.
 */
public class Constants {

    /**
     * 加密盐长度
     */
    public final static int SALT_LEN = 10;

    public final static int SUPER_PARENT_ID = 0;
}
