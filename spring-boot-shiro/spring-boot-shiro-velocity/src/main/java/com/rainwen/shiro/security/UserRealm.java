package com.rainwen.shiro.security;

import com.rainwen.shiro.entity.SysUser;
import com.rainwen.shiro.enums.UserStatus;
import com.rainwen.shiro.service.SysUserService;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.SimplePrincipalCollection;
import org.apache.shiro.util.ByteSource;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 用户认证器
 * <p>
 *
 * @author rain.wen
 * @date 2017/8/9
 */
public class UserRealm extends AuthorizingRealm {

    private SysUserService userService;

    private final Lock LOCK = new ReentrantLock(false);

    /**
     * 获取身份验证相关信息
     *
     * @param principals
     * @return
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        String username = (String) principals.getPrimaryPrincipal();
        SimpleAuthorizationInfo authorizationInfo = new SimpleAuthorizationInfo();
        LOCK.lock();
        try {
            //加载数据库权限配置
            authorizationInfo.setStringPermissions(userService.findResourcePermissions(username));
            authorizationInfo.addRoles(userService.findRoles(username));
        } finally {
            LOCK.unlock();
        }
        return authorizationInfo;
    }

    /**
     * 获取授权信息
     * @param token
     * @return
     * @throws AuthenticationException
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
        String username = (String) token.getPrincipal();
        SysUser user = userService.findByUsername(username);
        if (user == null) {
            //没找到帐号
            throw new UnknownAccountException();
        }
        if (UserStatus.LOCKED.getValue() == user.getStatus()) {
            //帐号锁定
            throw new LockedAccountException();
        }
        List<Object> principals = new ArrayList<Object>(2);
        principals.add(user.getUsername());
        principals.add(user);

        //实际在此处主要是进行密码匹配
        SimpleAuthenticationInfo authenticationInfo = new SimpleAuthenticationInfo();
        //密码验证通过此对象会放入作用域
        authenticationInfo.setPrincipals(new SimplePrincipalCollection(principals, getName()));
        //密码
        authenticationInfo.setCredentials(user.getPassword());
        //盐
        authenticationInfo.setCredentialsSalt(ByteSource.Util.bytes(user.getSalt()));
        return authenticationInfo;
    }

    public void setUserService(SysUserService userService) {
        this.userService = userService;
    }

    @Override
    public AuthorizationInfo getAuthorizationInfo(PrincipalCollection principals) {
        return super.getAuthorizationInfo(principals);
    }
}
