package com.rainwen.shiro.mapper;

import com.rainwen.shiro.entity.SysResource;
import com.rainwen.shiro.entity.SysResourceExample;
import com.rainwen.shiro.vo.UrlPermissionVO;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Set;

public interface SysResourceMapper {
    int countByExample(SysResourceExample example);

    int deleteByExample(SysResourceExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(SysResource record);

    int insertSelective(SysResource record);

    List<SysResource> selectByExample(SysResourceExample example);

    SysResource selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") SysResource record, @Param("example") SysResourceExample example);

    int updateByExample(@Param("record") SysResource record, @Param("example") SysResourceExample example);

    int updateByPrimaryKeySelective(SysResource record);

    int updateByPrimaryKey(SysResource record);

    @Select("select permission from t_sys_resource where id in (${resourceIds}) and permission != '' and available = true")
    Set<String> findResourcePermissions(@Param("resourceIds") String resourceIds);

    @Select("select url,permission from t_sys_resource where id in (${resourceIds}) and permission != '' and available = true")
    List<UrlPermissionVO> findUrlPermissions(@Param("resourceIds") String resourceIds);

    @Select("select id, name, type, url,permission, parent_id as parentId, icon from t_sys_resource where id in (${resourceIds}) and available = true order by parent_id asc, sort asc")
    List<SysResource> findResourcesByIds(@Param("resourceIds") String resourceIds);

}