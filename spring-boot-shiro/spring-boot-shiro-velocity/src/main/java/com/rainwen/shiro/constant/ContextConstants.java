package com.rainwen.shiro.constant;

/**
 * 常量
 * @author rain.wen
 * @date 2018/1/18.
 */
public class ContextConstants {

    public final static String SESSION_RESOURCE = "SESSION_RESOURCE";

    public final static String SESSION_URL_PERMISSION = "SESSION_URL_PERMISSION";

}
