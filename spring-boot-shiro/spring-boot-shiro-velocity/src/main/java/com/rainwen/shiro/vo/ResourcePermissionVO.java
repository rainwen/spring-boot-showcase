package com.rainwen.shiro.vo;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * 资源权限对象
 * @author rain.wen
 * @date 2018/1/18.
 */
public class ResourcePermissionVO {

    private List<ResourceVO> resourceVOList;

    private Map<String, String> urlPermissionMap;

    public List<ResourceVO> getResourceVOList() {
        return resourceVOList;
    }

    public void setResourceVOList(List<ResourceVO> resourceVOList) {
        this.resourceVOList = resourceVOList;
    }

    public Map<String, String> getUrlPermissionMap() {
        return urlPermissionMap;
    }

    public void setUrlPermissionMap(Map<String, String> urlPermissionMap) {
        this.urlPermissionMap = urlPermissionMap;
    }

    @Override
    public String toString() {
        return "ResourcePermissionVO{" +
                "resourceVOList=" + resourceVOList +
                ", urlPermissionMap=" + urlPermissionMap +
                '}';
    }
}
