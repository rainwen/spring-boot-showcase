package com.rainwen.shiro.controller;


import com.rainwen.shiro.entity.SysRoleResource;
import com.rainwen.shiro.page.PageRequest;
import com.rainwen.shiro.service.SysRoleService;
import com.rainwen.shiro.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author rain.wen
 * @date 2017/8/17
 */
@Controller
@RequestMapping("/sys/role")
public class RoleController extends BaseController {

    @Autowired
    private SysRoleService sysRoleService;

    /**
     * 查询列表
     * @return
     */
    @RequestMapping("/index")
    public String index(ModelMap modelMap, HttpServletRequest request) {
        PageRequest pageRequest = setPageRequest(request);
        modelMap.put("roles", sysRoleService.findRoles(pageRequest));
        modelMap.put("page", pageRequest);
        return "sys/role/list";
    }

    /**
     * 进入新增页
     * @return
     */
    @RequestMapping(value = "/create",method = RequestMethod.GET)
    public String create() {

        return "sys/role/create";
    }

    /**
     * 保存
     * @return
     */
    @RequestMapping(value = "/create",method = RequestMethod.POST)
    @ResponseBody
    public String save() {
        return "success";
    }

    /**
     * 进入编辑
     * @return
     */
    @RequestMapping(value = "/edit/{id}",method = RequestMethod.GET)
    public String edit(@PathVariable Integer id) {
        return "sys/role/edit";
    }

    /**
     * 编辑保存
     * @return
     */
    @RequestMapping(value = "/edit",method = RequestMethod.POST)
    @ResponseBody
    public String edit() {
        return "success";
    }

    /**
     * 删除
     * @return
     */
    @RequestMapping(value = "/delete/{id}",method = RequestMethod.POST)
    @ResponseBody
    public String delete(@PathVariable Integer id) {
        return "success";
    }

}
