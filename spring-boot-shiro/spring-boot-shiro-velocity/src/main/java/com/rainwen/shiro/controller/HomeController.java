package com.rainwen.shiro.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 首页
 *
 * @author rain.wen
 * @date 2018/1/16.
 */
@Controller
@RequestMapping("/home")
public class HomeController extends BaseController {

    @RequestMapping
    public String home(ModelMap modelMap) {
        return "home";
    }
}
