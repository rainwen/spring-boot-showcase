package com.rainwen.shiro.mapper;

import com.rainwen.shiro.entity.SysRoleResource;
import com.rainwen.shiro.entity.SysRoleResourceExample;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Set;

public interface SysRoleResourceMapper {
    int countByExample(SysRoleResourceExample example);

    int deleteByExample(SysRoleResourceExample example);

    int insert(SysRoleResource record);

    int insertSelective(SysRoleResource record);

    List<SysRoleResource> selectByExample(SysRoleResourceExample example);

    int updateByExampleSelective(@Param("record") SysRoleResource record, @Param("example") SysRoleResourceExample example);

    int updateByExample(@Param("record") SysRoleResource record, @Param("example") SysRoleResourceExample example);

    /**
     * 批量添加角色资源
     * @param roleId
     * @param resourceIds
     * @return
     */
    int batchInsert(@Param("roleId") Integer roleId, @Param("resourceIds") List<Integer> resourceIds);

    /**
     * 查询角色对应的资源ID
     * @param roleIds
     * @return
     */
    @Select("select rr.sys_resource_id from t_sys_role_resource rr where rr.sys_role_id in (${roleIds})")
    Set<Integer> findResourceIds(@Param("roleIds") String roleIds);
}