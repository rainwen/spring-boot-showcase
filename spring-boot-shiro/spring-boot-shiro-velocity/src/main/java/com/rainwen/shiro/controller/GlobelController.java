package com.rainwen.shiro.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;

/**
 * @author rain.wen
 * @date 2018/1/10.
 */
@Controller
public class GlobelController {

    /**
     * 尴尬！最开始访问404，自己犯傻了，没加视图
     * @return
     */
    @RequestMapping("/")
    public String index() {
        return "index";
    }

    /**
     * @return
     */
    @RequestMapping("/success")
    public String success() {
        return "success";
    }

    /**
     * @return
     */
    @RequestMapping("/unauthorized")
    public String unauthorized() {
        return "unauthorized";
    }

}
