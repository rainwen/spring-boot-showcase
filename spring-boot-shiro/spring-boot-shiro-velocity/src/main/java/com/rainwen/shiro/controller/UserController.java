package com.rainwen.shiro.controller;


import com.rainwen.shiro.page.PageRequest;
import com.rainwen.shiro.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author rain.wen
 * @date 2017/8/17
 */
@Controller
@RequestMapping("/sys/user")
public class UserController extends BaseController {

    @Autowired
    private SysUserService sysUserService;

    /**
     * 查询列表
     * @return
     */
    @RequestMapping("/index")
    public String index(ModelMap modelMap, HttpServletRequest request) {
        PageRequest pageRequest = setPageRequest(request);
        modelMap.put("users", sysUserService.findUsers(pageRequest));
        modelMap.put("page", pageRequest);
        return "sys/user/list";
    }

    /**
     * 进入新增页
     * @return
     */
    @RequestMapping(value = "/create",method = RequestMethod.GET)
    public String add() {

        return "sys/user/create";
    }

    /**
     * 保存
     * @return
     */
    @RequestMapping(value = "/create",method = RequestMethod.POST)
    @ResponseBody
    public String save() {
        return "success";
    }

    /**
     * 进入编辑
     * @return
     */
    @RequestMapping(value = "/update/{id}",method = RequestMethod.GET)
    public String edit(@PathVariable Integer id) {
        return "sys/user/update";
    }

    /**
     * 编辑保存
     * @return
     */
    @RequestMapping(value = "/update",method = RequestMethod.POST)
    @ResponseBody
    public String update() {
        return "success";
    }

    /**
     * 删除
     * @return
     */
    @RequestMapping(value = "/delete/{id}",method = RequestMethod.POST)
    @ResponseBody
    public String delete(@PathVariable Integer id) {
        return "success";
    }

    /**
     * 查看详情
     * @param id
     * @return
     */
    @RequestMapping(value = "/detail/{id}",method = RequestMethod.GET)
    public String detail(@PathVariable Integer id) {
        return "sys/user/view";
    }

    /**
     * 打印
     * @return
     */
    @RequestMapping(value = "/print",method = RequestMethod.GET)
    public String print() {
        return "sys/user/print";
    }
}
