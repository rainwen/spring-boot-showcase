package com.rainwen.shiro;


import com.rainwen.shiro.configuration.VelocityProperties;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.servlet.view.velocity.VelocityLayoutViewResolver;

/**
 * 启动入口
 * @author rain.wen
 * @date 2017/8/9
 *
 */
@SpringBootApplication
@MapperScan("com.rainwen.shiro.mapper")
@PropertySource({"classpath:application.properties"})
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean(name = "velocityViewResolver")
    public VelocityLayoutViewResolver velocityViewResolver(VelocityProperties properties) {
        VelocityLayoutViewResolver resolver = new VelocityLayoutViewResolver();
        properties.applyToViewResolver(resolver);
        resolver.setLayoutUrl("layout/default.vm");
        return resolver;
    }

}
