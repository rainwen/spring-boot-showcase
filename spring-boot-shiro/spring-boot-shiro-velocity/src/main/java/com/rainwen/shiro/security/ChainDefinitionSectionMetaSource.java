package com.rainwen.shiro.security;

import org.apache.shiro.config.Ini;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.FactoryBean;

/**
 * 借助spring {@link org.springframework.beans.factory.FactoryBean} 对apache shiro的premission进行动态创建
 * @author rain.wen
 * @date 2018/1/9.
 */
public class ChainDefinitionSectionMetaSource implements
        FactoryBean<Ini.Section> {

    /**
     * shiro默认的链接定义
     */
    private String filterChainDefinitions;

    /**
     * 通过filterChainDefinitions对默认的链接过滤定义
     *
     * @param filterChainDefinitions
     *            默认的链接过滤定义
     */
    public void setFilterChainDefinitions(String filterChainDefinitions) {
        this.filterChainDefinitions = filterChainDefinitions;
    }

    @Override
    public Ini.Section getObject() throws BeansException {
        Ini ini = new Ini();
        //配置文件url权限定义
        ini.load(filterChainDefinitions);
        Ini.Section section = ini.getSection(Ini.DEFAULT_SECTION_NAME);
        //默认所有的都需要登录
        section.put("/**", "authc,urlPermission");
        return section;
    }

    @Override
    public Class<?> getObjectType() {
        return Ini.Section.class;
    }

    @Override
    public boolean isSingleton() {
        return true;
    }
}
