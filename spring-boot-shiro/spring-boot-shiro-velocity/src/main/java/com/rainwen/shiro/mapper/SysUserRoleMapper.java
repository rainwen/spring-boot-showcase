package com.rainwen.shiro.mapper;

import com.rainwen.shiro.entity.SysUserRole;
import com.rainwen.shiro.entity.SysUserRoleExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SysUserRoleMapper {
    int countByExample(SysUserRoleExample example);

    int deleteByExample(SysUserRoleExample example);

    int insert(SysUserRole record);

    int insertSelective(SysUserRole record);

    List<SysUserRole> selectByExample(SysUserRoleExample example);

    int updateByExampleSelective(@Param("record") SysUserRole record, @Param("example") SysUserRoleExample example);

    int updateByExample(@Param("record") SysUserRole record, @Param("example") SysUserRoleExample example);

    /**
     * 批量添加用户角色
     * @param id
     * @param roleIds
     * @return
     */
    int batchInsert(@Param("id") Integer id, @Param("roleIds") List<Integer> roleIds);
}