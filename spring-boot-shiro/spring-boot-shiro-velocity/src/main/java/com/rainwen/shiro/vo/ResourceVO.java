package com.rainwen.shiro.vo;

import java.io.Serializable;
import java.util.List;

/**
 * 起源资源权限
 *
 * @author rain.wen
 * @date 2018/1/16.
 */
public class ResourceVO implements Serializable {

    private Integer id;
    /**
     * 名称
     */
    private String name;

    /**
     * 资源类型 {@link com.rainwen.shiro.enums.ResourceEnum}
     */
    private String type;

    /**
     * 访问地址
     */
    private String url;

    /**
     * 图标
     */
    private String icon;

    /**
     * 父节点
     */
    private Integer parentId;

    /**
     * 子权限资源
     */
    private List<ResourceVO> childResources;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public List<ResourceVO> getChildResources() {
        return childResources;
    }

    public void setChildResources(List<ResourceVO> childResources) {
        this.childResources = childResources;
    }

    @Override
    public String toString() {
        return "ResourceVO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", type='" + type + '\'' +
                ", url='" + url + '\'' +
                ", icon='" + icon + '\'' +
                ", parentId=" + parentId +
                ", childResources=" + childResources +
                '}';
    }
}
