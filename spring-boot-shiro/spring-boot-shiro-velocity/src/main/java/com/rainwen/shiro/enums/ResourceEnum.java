package com.rainwen.shiro.enums;

/**
 * 权限资源类型
 * @author rain.wen
 * @date 2018/1/16.
 */
public enum ResourceEnum {
    DIR,
    MENU,
    BUTTON
}
