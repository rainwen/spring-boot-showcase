package com.rainwen.shiro.mapper;

import com.rainwen.shiro.entity.SysResource;
import com.rainwen.shiro.entity.SysUser;
import com.rainwen.shiro.entity.SysUserExample;
import com.rainwen.shiro.page.PageRequest;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;
import java.util.Set;

public interface SysUserMapper {
    int countByExample(SysUserExample example);

    int deleteByExample(SysUserExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(SysUser record);

    int insertSelective(SysUser record);

    List<SysUser> selectByExample(SysUserExample example);

    SysUser selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") SysUser record, @Param("example") SysUserExample example);

    int updateByExample(@Param("record") SysUser record, @Param("example") SysUserExample example);

    int updateByPrimaryKeySelective(SysUser record);

    int updateByPrimaryKey(SysUser record);

    @Update("update t_sys_user set password=#{newPassword}, salt=#{salt} where id=#{id}")
    int changePassword(@Param("id") Integer id, @Param("newPassword") String newPassword, @Param("salt") String salt);

    @Select("select role.name from t_sys_user_role userole, t_sys_role role where userole.sys_user_id = #{id} and role.id = userole.sys_role_id")
    Set<String> findRoleNames(Integer id);

    @Select("select role.id from t_sys_user_role userole, t_sys_role role where userole.sys_user_id = #{id} and role.id = userole.sys_role_id")
    List<Integer> findRoleIds(Integer id);

    List<SysUser> findUsers(@Param("page")PageRequest pageRequest);

}