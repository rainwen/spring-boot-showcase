package com.rainwen.shiro.enums;

/**
 *
 * @author rain.wen
 * @date 2017/8/9
 */
public enum UserStatus {

    NORMAL(0),
    LOCKED(1),
    DISABLE(2);

    private int value;

    UserStatus(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
