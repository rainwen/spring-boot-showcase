package com.rainwen.shiro.security;

import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;

/**
 * 自定加密方式
 * @author rain.wen
 * @date 2017/8/11
 */
public class MyCredentialsMatcher extends HashedCredentialsMatcher {

    public MyCredentialsMatcher() {
        this.setHashAlgorithmName("MD5");
        this.setHashIterations(2);
        this.setStoredCredentialsHexEncoded(true);
    }

    @Override
    public boolean doCredentialsMatch(AuthenticationToken token, AuthenticationInfo info) {
        //自定其他验证策略
        return super.doCredentialsMatch(token, info);
    }
}
