package com.rainwen.shiro.vo;

import java.io.Serializable;

/**
 * Url权限
 * @author rain.wen
 * @date 2018/1/12.
 */
public class UrlPermissionVO implements Serializable {

    private String url;

    private String permission;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }
}
