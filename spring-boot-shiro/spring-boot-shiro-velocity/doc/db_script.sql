drop table if exists t_sys_user;

/*==============================================================*/
/* Table: t_sys_user                                            */
/*==============================================================*/
create table t_sys_user
(
   id                   int not null auto_increment comment '自增ID',
   username             varchar(32) not null comment '登录名',
   password             varchar(32) not null comment '密码',
   nickname             varchar(32) not null comment '昵称',
   salt                 varchar(32) not null comment '加密盐,系统随机',
   status               tinyint(2) not null default 0 comment '用户状态 0 正常 1 锁定 2 删除',
   create_time          datetime default CURRENT_TIMESTAMP comment '创建时间',
   last_update_time     datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '最后更新时间',
   primary key (id)
);

alter table t_sys_user comment '用户表';

/*==============================================================*/
/* Index: unique_username                                       */
/*==============================================================*/
create UNIQUE INDEX unique_username on t_sys_user
(
   username
);

drop table if exists t_sys_role;

/*==============================================================*/
/* Table: t_sys_role                                            */
/*==============================================================*/
create table t_sys_role
(
   id                   int not null auto_increment comment '自增ID',
   name                 varchar(32) not null comment '角色名称',
   description          varchar(64) comment '角色描述',
   available            tinyint(1) not null default 1 comment '是否可用 0 false | 1 true',
   primary key (id)
);

alter table t_sys_role comment '角色表';

drop table if exists t_sys_resource;

/*==============================================================*/
/* Table: t_sys_resource                                        */
/*==============================================================*/
CREATE TABLE `t_sys_resource` (
	`id` INT(11) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
	`name` VARCHAR(32) NOT NULL COMMENT '资源名称',
	`type` VARCHAR(32) NOT NULL COMMENT '资源类型: 目录 dir, 菜单menu, 按钮button',
	`sort` INT(11) NOT NULL DEFAULT '0' COMMENT '排序',
	`parent_id` INT(11) NOT NULL DEFAULT '0' COMMENT '父节点',
	`parent_ids` VARCHAR(128) NOT NULL DEFAULT '' COMMENT '父节点列表 0/1/2',
	`permission` VARCHAR(128) NOT NULL DEFAULT '' COMMENT '权限字符串',
	`url` VARCHAR(128) NOT NULL DEFAULT '' COMMENT '资源链接:格式/user/save',
	`icon` VARCHAR(64) NOT NULL DEFAULT '' COMMENT '菜单或按钮图标',
	`available` TINYINT(1) NOT NULL DEFAULT '1' COMMENT '是否可用 0 false 1 true',
	`create_time` DATETIME NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
	`last_update_time` DATETIME NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
	PRIMARY KEY (`id`)
)

alter table t_sys_resource comment '系统资源表';

drop table if exists t_sys_user_role;

/*==============================================================*/
/* Table: t_sys_user_role                                       */
/*==============================================================*/
create table t_sys_user_role
(
   sys_user_id          int not null comment '用户ID',
   sys_role_id          int not null comment '角色ID'
);

alter table t_sys_user_role comment '用户角色表';

/*==============================================================*/
/* Index: unique_sys_user_id_sys_role_id                        */
/*==============================================================*/
create unique index unique_sys_user_id_sys_role_id on t_sys_user_role
(
   sys_user_id,
   sys_role_id
);


drop table if exists t_sys_role_resource;

/*==============================================================*/
/* Table: t_sys_role_resource                                   */
/*==============================================================*/
create table t_sys_role_resource
(
   sys_role_id          int not null comment '角色ID',
   sys_resource_id      int not null comment '资源ID'
);

alter table t_sys_role_resource comment '角色资源表';

/*==============================================================*/
/* Index: unique_sys_role_id_sys_resource_id                    */
/*==============================================================*/
create unique index unique_sys_role_id_sys_resource_id on t_sys_role_resource
(
   sys_role_id,
   sys_resource_id
);




