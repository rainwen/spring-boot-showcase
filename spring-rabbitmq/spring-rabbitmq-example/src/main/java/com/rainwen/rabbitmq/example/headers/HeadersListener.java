package com.rainwen.rabbitmq.example.headers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.UnsupportedEncodingException;

/**
 * Created by rain.wen on 2017/7/22.
 */
@Component
@Scope("prototype")
public class HeadersListener implements MessageListener {

    private final static Logger logger = LoggerFactory.getLogger(HeadersListener.class);

    @Override
    public void onMessage(Message message) {
        try {
            String messageStr = new String(message.getBody(), "UTF-8");
            logger.info("==> header {}", messageStr);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

    }
}
