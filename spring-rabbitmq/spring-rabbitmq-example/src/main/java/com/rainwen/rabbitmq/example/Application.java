package com.rainwen.rabbitmq.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.*;

import java.util.concurrent.CountDownLatch;

/**
 * Created by rain.wen on 2017/07/19.
 */
@SpringBootApplication
@ComponentScan("com.rainwen")
@PropertySource({"classpath:application.properties"})
@ImportResource("classpath:rabbitmq.xml")
public class Application {

    public static void main(String[] args) throws InterruptedException {
        SpringApplication.run(Application.class, args);
        new CountDownLatch(1).await();
    }
}
