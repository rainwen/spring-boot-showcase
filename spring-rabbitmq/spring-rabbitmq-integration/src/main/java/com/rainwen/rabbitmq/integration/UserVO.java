package com.rainwen.rabbitmq.integration;

import java.io.Serializable;

/**
 * Created by rain.wen on 2017/7/23.
 */
public class UserVO implements Serializable {

    private String name;

    private String password;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
