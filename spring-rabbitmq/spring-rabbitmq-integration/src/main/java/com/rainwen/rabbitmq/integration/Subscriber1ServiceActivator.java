package com.rainwen.rabbitmq.integration;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.messaging.support.GenericMessage;

public class Subscriber1ServiceActivator {

	private static Log logger = LogFactory
			.getLog(Subscriber1ServiceActivator.class);

	public void logXml(GenericMessage<UserVO> msg) throws Exception {
		String text = msg.toString();
		logger.info("### 订阅1 ###" + text);
	}
}