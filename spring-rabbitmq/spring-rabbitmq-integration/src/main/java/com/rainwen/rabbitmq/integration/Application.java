package com.rainwen.rabbitmq.integration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.PropertySource;

import java.util.concurrent.CountDownLatch;

/**
 * Created by rain.wen on 2017/07/19.
 */
@SpringBootApplication
@PropertySource({"classpath:application.properties"})
@ImportResource("classpath:rabbitmq-integration.xml")
public class Application {

    public static void main(String[] args) throws InterruptedException {
        SpringApplication.run(Application.class, args);
        new CountDownLatch(1).await();
    }
}
