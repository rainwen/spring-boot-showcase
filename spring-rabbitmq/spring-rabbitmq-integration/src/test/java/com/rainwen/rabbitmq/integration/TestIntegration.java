package com.rainwen.rabbitmq.integration;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


/**
 * Created by wenjl on 2017/7/23.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class TestIntegration {

    @Autowired
    @Qualifier("pub-sub-channel")
    MessageChannel channel;

    @Test
    public void send() throws InterruptedException {

        for (int i = 0; i < 10; i++) {
            UserVO vo = new UserVO();
            vo.setName("rainwen");
            vo.setPassword("123456");
            Message<UserVO> message = MessageBuilder.withPayload(vo)
                    .setHeader("type", "json")
                    .build();
            channel.send(message);
        }
    }
}
