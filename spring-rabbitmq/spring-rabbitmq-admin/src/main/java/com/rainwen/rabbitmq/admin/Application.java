package com.rainwen.rabbitmq.admin;

import org.springframework.boot.SpringApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.PropertySource;

/**
 * Created by rain.wen on 2017/07/19.
 */
@Configuration
@PropertySource({"classpath:application.properties"})
@ImportResource("classpath:rabbitmq*.xml")
public class Application {

    @Bean
    ApplicationPostProcessor applicationPostProcessor(){
        return new ApplicationPostProcessor();
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
