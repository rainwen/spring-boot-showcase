package com.rainwen.rabbitmq.admin;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;

import java.io.UnsupportedEncodingException;

public class InitQueueListener implements MessageListener {
    @Override
    public void onMessage(Message message) {
        try {
            System.out.println("test:" + new String(message.getBody(), "utf-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }
}
