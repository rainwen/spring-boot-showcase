package com.rainwen.rabbitmq.admin;

import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;

public class ApplicationPostProcessor implements ApplicationListener<ContextRefreshedEvent>
{
	@Override
	public void onApplicationEvent(ContextRefreshedEvent event)
	{  
	    if(event.getApplicationContext().getParent() == null){
			try {
				Thread.sleep(3000); //容器启动后执行
				System.out.println("==> success");
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			System.exit(0);
		}
	} 
	
}