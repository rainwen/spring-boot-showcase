package com.rainwen.rabbitmq.server.service;

import java.util.Map;
import java.util.Properties;

/**
 * Created by wenjl on 2017/07/19.
 */
public interface MessageService {

    /**
     * 发送消息
     * @param exchange 交换区
     * @param routingKey routingKey值
     * @param object 对象
     * @param header 头属性
     */
    void send(String exchange, String routingKey, Object object, final Map header);

    /**
     * 发送消息
     * @param exchange 交换区
     * @param routingKey routingKey值
     * @param object 对象
     * @param header 头属性
     * @param expires 过期时间 毫秒
     */
    void send(String exchange, String routingKey, Object object, final Map header, long expires);

    /**
     * 创建并绑定一个队列到一个交换,并且制定对应的路由键
     * @param exchange
     * @param queueName
     * @param routingKey
     */
    void declareExchangeAndQueue(String exchange, String queueName, String routingKey);

    /**
     * 删除一个交换
     * @param exchangeName
     * @return
     */
    boolean deleteExchange(String exchangeName);

    /**
     * 删除一个队列
     * @param queueName
     * @param unused
     * @param empty
     */
    void deleteQueue(String queueName, boolean unused, boolean empty);

    /**
     * 清空一个队列
     * @param queueName
     * @param noWait
     */
    void purgeQueue(String queueName, boolean noWait);

    /**
     * 获取一个队列的属性
     * @param queueName
     * @return
     */
    Properties getQueueProperties(String queueName);

}
