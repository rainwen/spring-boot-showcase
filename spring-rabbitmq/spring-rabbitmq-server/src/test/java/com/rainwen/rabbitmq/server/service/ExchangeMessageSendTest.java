package com.rainwen.rabbitmq.server.service;

import com.alibaba.fastjson.JSONObject;
import com.rainwen.rabbitmq.server.Application;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Created by rain.wen on 2017/7/22.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
public class ExchangeMessageSendTest {

    @Autowired
    private MessageService messageService;


    @Test
    public void directExchangeSendTest() throws InterruptedException {
        String exchange = "rainwen.direct.exchange";
        String routingKey = "rainwen.direct.exchange.queue";
        String routingKey2 = "rainwen.direct.exchange.queue2";
        JSONObject message = new JSONObject();
        message.put("exchange", exchange);
        message.put("queue", routingKey);

        JSONObject message2 = new JSONObject();
        message2.put("exchange", exchange);
        message2.put("queue", routingKey2);

        //发送direct 模式的消息，2个监听队列分别消费各自消息
        messageService.send(exchange, routingKey, message, null);
        messageService.send(exchange, routingKey2, message2, null);
    }

    @Test
    public void fanoutExchangeSendTest() throws InterruptedException {
        String exchange = "rainwen.fanout.exchange";
        String routingKey = "rainwen.fanout.exchange.queue";
        String routingKey2 = "rainwen.fanout.exchange.queue2";
        JSONObject message = new JSONObject();
        message.put("exchange", exchange);
        message.put("queue", routingKey);

        JSONObject message2 = new JSONObject();
        message2.put("exchange", exchange);
        message2.put("queue", routingKey2);

        //发送fanout 模式的消息，2个监听队列分别会消费2条消息
        messageService.send(exchange, routingKey, message, null);
        messageService.send(exchange, routingKey2, message2, null);
    }

    @Test
    public void headersExchangeSendTest() throws InterruptedException {
        String exchange = "rainwen.headers.exchange";
        String routingKey = "rainwen.headers.exchange.queue";
        JSONObject message = new JSONObject();
        message.put("exchange", exchange);
        message.put("queue", routingKey);

        Map<String,String> headers = new HashMap<String, String>(1);
        headers.put("rainwen", "123456");//!重要
        //发送headers 模式的消息，必须保证头信息正确，否则消息将会丢弃
        messageService.send(exchange, routingKey, message, headers);
    }

    @Test
    public void topicExchangeSendTest() throws InterruptedException {
        String exchange = "rainwen.topic.exchange";
        String routingKey = "rainwen.topic.exchange.queue";
        String routingKey2 = "rainwen.topic.exchange.queue2";
        JSONObject message = new JSONObject();
        message.put("exchange", exchange);
        message.put("queue", routingKey);

        JSONObject message2 = new JSONObject();
        message2.put("exchange", exchange);
        message2.put("queue", routingKey2);

        //发送topic 模式的消息, rainwen.topic.exchange.all 队列会同时受到2条消息
        messageService.send(exchange, routingKey, message, null);
        messageService.send(exchange, routingKey2, message2, null);
    }
}
