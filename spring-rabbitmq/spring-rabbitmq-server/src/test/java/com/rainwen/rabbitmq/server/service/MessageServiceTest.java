package com.rainwen.rabbitmq.server.service;

import com.alibaba.fastjson.JSONObject;
import com.rainwen.rabbitmq.server.Application;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.PropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.Assert;

import java.util.Properties;

/**
 * Created by rain.wen on 2017/7/20.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@PropertySource({"classpath:application.properties"})
public class MessageServiceTest {

    @Autowired
    private MessageService messageService;

    @Test
    public void declareExchangeAndQueueTest() {
        String exchange = "rainwen.test";
        String queueName = "rainwen.test.all";
        String routingKey = "rainwen.test.*";
        messageService.declareExchangeAndQueue(exchange, queueName, routingKey);
        Properties properties = messageService.getQueueProperties(queueName);
        Assert.isTrue(properties.getProperty(RabbitAdmin.QUEUE_NAME.toString()).equals(queueName), "must be true");
    }

    @Test
    public void sendTest() {
        String exchange = "rainwen.test";
        String queueName = "rainwen.test.all";
        JSONObject message = new JSONObject();
        message.put("id", "001");
        message.put("name", "rainwen");
        messageService.send(exchange, queueName, message, null);
        Properties properties = messageService.getQueueProperties(queueName);
        Object obj = properties.get(RabbitAdmin.QUEUE_MESSAGE_COUNT.toString());
        Assert.isTrue(Integer.parseInt(obj + "") > 0, "must be true");
    }
}
