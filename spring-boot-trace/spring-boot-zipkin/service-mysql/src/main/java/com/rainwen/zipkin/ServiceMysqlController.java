package com.rainwen.zipkin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ServiceMysqlController {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @GetMapping("/mysqltest")
    public String mysqlTest() {
        String name = jdbcTemplate.queryForObject("SELECT name FROM user WHERE id = 1", String.class);
        return "Welcome " + name;
    }

}