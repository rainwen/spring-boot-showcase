### Spring-Boot 集成Zipkin

> Zipkin 是 Twitter 的一个开源项目，允许开发者收集 Twitter 各个服务上的监控数据，并提供查询接口。

项目源码主要实现功能：
- Zipkin 追踪3个微服务调用链
- Zipkin 追踪MySQL调用链


### Zipkin 安装启动：

1. 进入官方 [快速入门](https://zipkin.io/pages/quickstart.html)
2. 找到自己对应安装方式: Docker、下载执行包，源码安装
> 我本地直接下载可执行包，环境要求Java 8或以上 [下载可执行jar包](https://search.maven.org/remote_content?g=io.zipkin.java&a=zipkin-server&v=LATEST&c=exec)
3. 执行启动
```
java -jar zipkin-server-2.8.3-exec.jar

                                    ********
                                  **        **
                                 *            *
                                **            **
                                **            **
                                 **          **
                                  **        **
                                    ********
                                      ****
                                      ****
        ****                          ****
     ******                           ****                                 ***
  ****************************************************************************
    *******                           ****                                 ***
        ****                          ****
                                       **
                                       **


             *****      **     *****     ** **       **     **   **
               **       **     **  *     ***         **     **** **
              **        **     *****     ****        **     **  ***
             ******     **     **        **  **      **     **   **

:: Powered by Spring Boot ::         (v2.0.1.RELEASE)

```

启动成功，直接访问 http://localhost:9411 进入 Zipkin后台

### 微服务调用监控
1.启动3个为服务
 >service-a http://localhost:8881/service-a<br/>
 >service-b http://localhost:8882/service-b<br/>
 >service-c http://localhost:8883/service-c<br/>

 调用过程：service-a -> service-b -> service-c，每个调用休息3秒

 实例代码：
 ```
 @GetMapping("/servicea")
     public String service() {
         try {
             Thread.sleep( 3000 );
         } catch (InterruptedException e) {
             e.printStackTrace();
         }
         return restTemplate.getForObject("http://localhost:8882/serviceb", String.class);
     }
 ```

2.请求 http://localhost:8881/service-a
等待9秒，得到结果
> Now, we reach the terminal call: servicec !

访问 http://localhost:9411 查看具体调用链

### MySQL调用监控

1.环境准备
- 创建zipkin数据库
- 创建test数据库

[初始化脚本](https://gitee.com/rainwen/spring-boot-showcase/blob/master/spring-boot-trace/spring-boot-zipkin/mysql/src/main/resources/dbscript.sql)

2. Zipkin 追踪 MySQL 调用链启动方式
```
java -jar zipkin-server-2.8.3-exec.jar --STORAGE_TYPE=mysql --MYSQL_DB=zipkin --MYSQL_USER=root --MYSQL_PASS=root --MYSQL_HOST=localhost --MYSQL_TCP_PORT=3306
```

3. 启动服务
 > service-mysql http://localhost:8884

4. 请求 http://localhost:8884/mysqltest

得到结果：Welcome rainwen

```
@GetMapping("/mysqltest")
    public String mysqlTest() {
        String name = jdbcTemplate.queryForObject("SELECT name FROM user WHERE id = 1", String.class);
        return "Welcome " + name;
    }
```

访问 http://localhost:9411 查看具体调用链


项目源码参考：https://www.jianshu.com/p/da80ea881424