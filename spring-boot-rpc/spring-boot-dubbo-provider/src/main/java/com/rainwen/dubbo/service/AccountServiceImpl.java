package com.rainwen.dubbo.service;

import com.rainwen.api.dto.AccountDTO;
import com.rainwen.api.service.AccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.math.BigInteger;

/**
 * @author rain.wen
 * @date 2018/3/19.
 */
@Service
public class AccountServiceImpl implements AccountService {

    private static final Logger logger = LoggerFactory.getLogger(AccountServiceImpl.class);

    @Override
    public AccountDTO getAccountById(Long id) {
        AccountDTO accountDTO = new AccountDTO();
        accountDTO.setId(1L);
        accountDTO.setAmount(new BigInteger("1000"));
        accountDTO.setBalance(new BigInteger("1000"));
        accountDTO.setFrozen(new BigInteger("0"));
        logger.info("查询成功");
        return accountDTO;
    }
}
