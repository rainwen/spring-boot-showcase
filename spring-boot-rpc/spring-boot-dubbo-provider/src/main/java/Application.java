import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.PropertySource;

import java.util.concurrent.CountDownLatch;

/**
 * @author rain.wen
 * @date 2018/3/19.
 */

@SpringBootApplication
@PropertySource("classpath:application.properties")
@ImportResource("classpath:dubbo.xml")
@ComponentScan("com.rainwen")
public class Application {

    public static void main(String[] args) {

        SpringApplication.run(Application.class, args);

        try {
            new CountDownLatch(1).await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
