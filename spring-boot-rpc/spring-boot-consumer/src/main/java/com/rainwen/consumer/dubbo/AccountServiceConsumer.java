package com.rainwen.consumer.dubbo;

import com.rainwen.api.service.AccountService;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author rain.wen
 * @date 2018/3/19.
 */
@Service
public class AccountServiceConsumer {

    @Autowired
    private AccountService accountService;

    public void getAccountByIdTest() {
        Assert.assertTrue(accountService.getAccountById(1L).getId() == 1L);
    }
}
