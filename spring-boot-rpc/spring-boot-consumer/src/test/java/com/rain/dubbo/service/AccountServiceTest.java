package com.rain.dubbo.service;

import com.rainwen.api.dto.AccountDTO;
import com.rainwen.consumer.Application;
import com.rainwen.consumer.hystrix.AccountServiceHystrix;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @author rain.wen
 * @date 2018/3/19.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
public class AccountServiceTest {

    @Autowired
    private AccountServiceHystrix accountService;

    @Test
    public void getAccountByIdTest() throws InterruptedException {
        while (true){
            AccountDTO accountDTO = accountService.getAccountById(1L);
            Assert.assertTrue(accountDTO != null);
            System.out.println(accountDTO.toString());

            Thread.sleep(2*1000);
        }
    }
}
