package com.rain.dubbo.service;

import com.rainwen.api.dto.AccountDTO;
import com.rainwen.api.service.AccountService;
import com.rainwen.consumer.Application;
import com.rainwen.consumer.hystrix.AccountServiceHystrix;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @author rain.wen
 * @since 2018/7/3 19:23
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
public class MockAccountServiceTest {

    @SpyBean //执行代码逻辑
//    @MockBean //不执行代码逻辑
    AccountService accountService;

    @Autowired
    AccountServiceHystrix accountServiceHystrix;

    @Test
    public void getAccountByIdTest() throws InterruptedException {
        Long id = 1L;
        AccountDTO accountDTO = new AccountDTO();
        accountDTO.setId(id);
        Mockito.when(accountService.getAccountById(id)).thenReturn(accountDTO);
        Assertions.assertThat(accountDTO.getId()).isEqualTo(id);

        accountServiceHystrix.getAccountById(id);
    }
}
