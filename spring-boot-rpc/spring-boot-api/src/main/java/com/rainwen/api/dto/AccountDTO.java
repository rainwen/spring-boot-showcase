package com.rainwen.api.dto;

import java.io.Serializable;
import java.math.BigInteger;

/**
 * 账户对象
 * @author rain.wen
 * @date 2018/3/19.
 */
public class AccountDTO implements Serializable {

    /**
     * ID
     */
    private Long id;

    /**
     * 姓名
     */
    private String name;

    /**
     * 总金额
     */
    private BigInteger amount;

    /**
     * 余额
     */
    private BigInteger balance;

    /**
     * 冻结金额
     */
    private BigInteger frozen;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigInteger getAmount() {
        return amount;
    }

    public void setAmount(BigInteger amount) {
        this.amount = amount;
    }

    public BigInteger getBalance() {
        return balance;
    }

    public void setBalance(BigInteger balance) {
        this.balance = balance;
    }

    public BigInteger getFrozen() {
        return frozen;
    }

    public void setFrozen(BigInteger frozen) {
        this.frozen = frozen;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("AccountDTO{");
        sb.append("id=").append(id);
        sb.append(", name='").append(name).append('\'');
        sb.append(", amount=").append(amount);
        sb.append(", balance=").append(balance);
        sb.append(", frozen=").append(frozen);
        sb.append('}');
        return sb.toString();
    }
}
