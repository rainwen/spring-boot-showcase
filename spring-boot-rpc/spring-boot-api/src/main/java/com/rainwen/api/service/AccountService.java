package com.rainwen.api.service;

import com.rainwen.api.dto.AccountDTO;

/**
 * 账户接口
 * @author rain.wen
 * @date 2018/3/19.
 */
public interface AccountService {

    AccountDTO getAccountById(Long id);
}
