package com.rainwen.jsp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by rain.wen on 2017/8/10.
 */
@Controller
public class JspController {

    @RequestMapping("/success")
    public String success(ModelMap modelMap) {
        modelMap.addAttribute("message","Hello Spring Boot JSP!!");
        return "success";
    }
}
