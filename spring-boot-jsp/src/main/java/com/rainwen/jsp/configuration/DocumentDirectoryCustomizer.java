package com.rainwen.jsp.configuration;

import org.springframework.boot.context.embedded.ConfigurableEmbeddedServletContainer;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.context.annotation.Configuration;

import java.io.File;


/**
 * 内嵌Tomcat 运行项目目录 不推荐这种写法，
 * 推荐使用IDEA 设置 work directory
 *
 * 这种方式可以实现资源文件目录自定义
 *
 * @author rain.wen
 */
public class DocumentDirectoryCustomizer implements EmbeddedServletContainerCustomizer {
    public void customize(ConfigurableEmbeddedServletContainer container) {
        //项目目录
        container.setDocumentRoot(new File(System.getProperty("user.dir") + "/spring-boot-jsp/src/main/webapp"));
    }
}