> 项目主要记录Spring Boot + JSP 内嵌 Tomcat 启动访问JSP 报 Whitelabel Error Page (type=Not Found, status=404)

Spring Boot [Quick Start](http://projects.spring.io/spring-boot/) 提供的事例非常简单，相信大家都和我一样运行 “Run SampleController” 得到 Hello World!。

不过这个实例虽然简单，但在运行包含JSP页面时候还是隐藏了不少坑。

## 环境配置
- Spring Boot 1.5.3.RELEASE 

### Maven 依赖
    
    <dependencies>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
    </dependencies>
    
### application.properties 配置

    # port
    server.port= 8080
    
    # jsp
    spring.mvc.view.prefix= /WEB-INF/views/
    spring.mvc.view.suffix= .jsp
    
### Controller

    [@Controller](https://my.oschina.net/u/1774615)
    public class JspController {

        @RequestMapping("/success")
        public String success(ModelMap modelMap) {
            modelMap.addAttribute("message","Hello Spring Boot JSP!!");
            return "success";
        }
    }

### JSP

    <%@ page contentType="text/html;charset=UTF-8" language="java" %>
    <html>
    <head>
        <title></title>
    </head>
    <body>
      ${message}
    </body>
    </html>


### Spring Boot启动类

    /**
     * Created by rain.wen on 2017/8/9.
     */
    @SpringBootApplication
    public class Application {

        public static void main(String[] args) {
            SpringApplication.run(Application.class, args);
        }

    }

OK，全部配置好 “Run Application”，启动日志一切正常，访问 http://localhost:8080/success

- 理想

```
    Hello Spring Boot JSP!!
```

- 现实

```
    Whitelabel Error Page

    This application has no explicit mapping for /error, so you are seeing this as a fallback.

    Thu Aug 10 12:06:31 CST 2017
    There was an unexpected error (type=Not Found, status=404).
    /WEB-INF/views/success.jsp
```

内心是一顿懵逼，查阅不少资料可能还是没有解决。

弄了半天还是这个样，麻痹！！！，难道Spring Boot 不支持这种，这怎么可能。

最后通过改变关键字搜索，最终找到一遍靠谱的：[使用spring boot和embedded tomcat开发时出现404找不到jsp问题的处理](https://www.iflym.com/index.php/java-programe/201706060001.html)

## 解决问题 JSP 404
结合文章，调试代码

> TomcatEmbeddedServletContainerFactory.prepareContext  这个方法确实有蹊跷

     File docBase = this.getValidDocumentRoot(); 结果 docBase = Null
     docBase = docBase != null?docBase:this.createTempDir("tomcat-docbase");  因为 docBase = Null 自动建立临时目录 tomcat-docbase

     docBase 目录为 Tomcat 运行项目目录， 自动创建的临时目录肯定就找不到JSP啦

运行截图：

![运行截图](https://static.oschina.net/uploads/img/201708/11192703_0nsf.png "运行截图")

### 加上2步完美解决

1. 程序中指定docBase 运行目录：

```
    /**
     * 内嵌Tomcat 运行项目目录
     */
    @Configuration
    public class DocumentDirectoryCustomizer implements EmbeddedServletContainerCustomizer {
        public void customize(ConfigurableEmbeddedServletContainer container) {
            //项目目录
            container.setDocumentRoot(new File(System.getProperty("user.dir") + "/spring-boot-jsp/src/main/webapp"));
        }
    }
```
或者
如果使用IDEA 指定work directory 目录为$MODULE_DIR$，省去手动指定，如图：

![IDEA 设置](https://static.oschina.net/uploads/img/201708/11192738_axQs.png "IDEA 设置")

(推荐使用编辑器设置，代码写死后面打包后，路径又不一样  20180507 补充)

2.  **！！！重要** Maven 加入 JSP 运行依赖包

        <!-- 使用 jsp 必要引入 -->
        <dependency>
            <groupId>org.apache.tomcat.embed</groupId>
            <artifactId>tomcat-embed-jasper</artifactId>
        </dependency>


完成以上2步，再次 “Run Application”，访问 http://localhost:8080/success
可以得到理想的：Hello Spring Boot JSP!!

## 打包为可执行jar包 (20180507补充)

增加 spring-boot-maven-plugin 插件
```
<plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
                <configuration>
                    <!-- 输出文件名添加exec  -->
                    <classifier>exec</classifier>
                </configuration>
            </plugin>
```

执行打包
```
mvn package

[INFO] Attaching archive: D:\IdeaProjects\spring-boot-showcase\spring-boot-jsp\target\spring-boot-jsp-1.0-SNAPSHOT-exec.jar, with classifier: exec
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 2.410 s
[INFO] Finished at: 2018-05-07T16:06:40+08:00
[INFO] Final Memory: 16M/39M
[INFO] ------------------------------------------------------------------------

```

运行可执行jar包
```
java -jar ./target/spring-boot-jsp-1.0-SNAPSHOT-exec.jar
```

## 代码

[spring-boot-jsp](http://git.oschina.net/rainwen/spring-boot-showcase/tree/master/spring-boot-jsp)

end